# Building FSL conda packages locally


This document describes how to build a conda package for an FSL project from
the corresponding conda recipe.


It does not describe how to *create* a conda recipe for a FSL project - that
is described in [`fsl_conda_recipes.md`](fsl_conda_recipes.md).


Normally there should be no need to build FSL conda packages by hand, as
packages are automatically built and published using Gitlab CI. However the
need may arise for development, testing, or debugging purposes, and hence the
process is described here.


In order to build a FSL conda package locally, all you need is a (mini)conda
environment with `conda-build` installed. You do not need to have a compiler,
or even FSL, installed. If you are working with internal-only FSL conda
packages, you may also need a username and password to access the internal
FSL conda channel.


> **Note:** If you are building a CUDA package, you need to have a CUDA
> Toolkit installed, and the `nvcc` executable available on your `$PATH`.


## Step 1: Clone the recipe repository


To start, you need to create a local clone of the recipe repository. For
example, if you would like to build a package for `fsl/avwutils`:


```bash
git clone https://git.fmrib.ox.ac.uk/fsl/conda/fsl-avwuitls
```


## Step 2: Clone the project repository (optional)


By default, the `fsl-avwutils` recipe will build a package from the
`fsl/avwutils` gitlab repository. If you would like to build a package from a
*local* clone, or a personal fork, of `fsl/avwutils`, you can use the
`FSLCONDA_REPOSITORY` variable to override the default setting (which is to
build from the gitlab `fsl/avwutils` project):


```bash
git clone https://git.fmrib.ox.ac.uk/fsl/avwuitls.git
export FSLCONDA_REPOSITORY=$(pwd)/avwutils
```


> **Note:** It is not currently possible to build a conda package from a local
> copy of a project which is *not* a git repository, nor from a dirty working
> tree (i.e. the changes you want to build must be committed). This
> functionality may be added in the future if it is deemed necessary.


## Step 3: Choose the revision you want to build (optional)


By default, the `fsl-avwutils` conda recipe will build a package from the last
released version (tag) on the `fsl/avwutils` gitlab repository - this is
specified in in the recipe `meta.yaml`. If you would like to build a package
for a different release, or for a specific branch, you can set the
`FSLCONDA_REVISION` variable to override the default setting. For example, if
you want to build a package for the `master` branch:


```bash
export FSLCONDA_REVISION=master
```


## Step 4: Run `conda build` to build the package


Now you can run `conda build` to build the conda package. The channel order is
very important, and must be:

1. The public FSL conda channel has the highest priority.
2. `conda-forge` Packages from conda-forge have lower priority.


```bash
cd fsl-avwutils
conda build                                                            \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/public/ \
  -c conda-forge                                                       \
  --output-folder=dist                                                 \
  .
```


If your project depends on internal-only FSL packages, add the internal FSL
conda channel **before** the public channel, like so (set the
`${FSLCONDA_USERNAME}` and `${FSLCONDA_PASSWORD}` environment variables to the
username/password for the internal FSL conda channel):


```bash
cd fsl-avwutils
conda build                                                                                              \
  -c https://${FSLCONDA_USERNAME}:${FSLCONDA_PASSWORD@fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/internal/ \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/                                            \
  -c conda-forge                                                                                         \
  --output-folder=dist                                                                                   \
  .
```


Similarly, if you wish to build your package using development versions of
other FSL conda packages, specify the FSL development conda channel before
the public channel, e.g.:

```bash
conda build                                                        \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/ \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/      \
  -c conda-forge                                                   \
  --output-folder=dist                                             \
  .
```


If the build succeeds, the built package will be saved to the `dist`
directory - binary packages will be saved in either the `osx-64` or `linux-64`
sub-directories, and platform-independent (e.g. python) packages will be saved
in the `noarch` sub-directory.


## Building multiple FSL conda packages locally


The need may arise to build conda packages for multiple FSL projects at
once. For example, you might be making changes to one project which depends on
changes you have made to another project (a dependency of the first
project). In this case it makes sense to set up a local conda channel, and to
build/install the dependencies into that channel.


Let's say we are working on the `randomise` project, and are simultaneously
making changes to the `newimage` project. We will need local clones of all the
project and recipe repositories:


```bash
git clone https://git.fmrib.ox.ac.uk/fsl/randomise.git
git clone https://git.fmrib.ox.ac.uk/fsl/newimage.git
git clone https://git.fmrib.ox.ac.uk/fsl/conda/fsl-randomise.git
git clone https://git.fmrib.ox.ac.uk/fsl/conda/fsl-newimage.git
```


Before we can build a conda package for `randomise`, we need to build a conda
package from our local development version of `newimage`. We use
`FSLCONDA_REPOSITORY` and `FSLCONDA_REVISION` to direct the build to use our
local `newimage` repository, and direct the build to `my_local_conda_channel`,
which we will use later.


```bash
mkdir my_local_conda_channel

export FSLCONDA_REPOSITORY=$(pwd)/newimage
export FSLCONDA_REVISION=enh/my_local_newimage_development_branch

conda build                                                   \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ \
  -c conda-forge                                              \
  --output-folder=./my_local_conda_channel                    \
  ./fsl-newimage
```


Now we can build our development version of `randomise`, using the development
conda package we just built for `newimage`, simply by adding our local channel
directory as a conda channel, making sure to list it *before* the FMRIB conda
channel URL, so that it takes precedence:


```bash
export FSLCONDA_REPOSITORY=$(pwd)/randomise
export FSLCONDA_REVISION=enh/my_local_randomimse_development_branch

conda build                                                   \
  -c file://$(pwd)/my_local_conda_channel                     \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ \
  -c conda-forge                                              \
  --output-folder=./my_local_conda_channel                    \
  ./fsl-randomise
```
