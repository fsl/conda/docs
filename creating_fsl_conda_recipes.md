# Writing a conda recipe for a FSL project


This document describes how to write a conda recipe for a FSL project. All FSL
project repositories have an associated recipe repository, which contains
metadata describing how to build a conda recipe from the project repository.


You can use a script to automatically generate an initial version of a
repository for your project (recommended), or you can create a conda recipe by
hand.


> **Note:** It is recommended to use the `create_conda_recipe` command (see
> below) to generate your recipe, or to use an existing FSL conda recipe as
> the basis for your recipe. The automatic CI rules assume that recipes adhere
> to a particular format - if the structure of your recipe deviates from this,
> it may not be able to be automatically updated.


Conda recipes for all FSL projects, and some internally packaged/managed
third-party dependencies, are hosted in the GitLab
[fsl/conda](https://git.fmrib.ox.ac.uk/fsl/conda/) namespace. The name of a
FSL conda recipe repository is the same as the name of the FSL conda package -
for example, the conda package for the
[`fsl/avwutils`](https://git.fmrib.ox.ac.uk/fsl/avwutils/) project is named
`fsl-avwutils`, and is hosted at
[`fsl/conda/fsl-avwutils`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-avwutils/).


## FSL conda package naming conventions


FSL conda package names must follow the [conda package naming
conventions](https://conda.io/projects/conda-build/en/latest/concepts/package-naming-conv.html#index-0),
and be comprised solely of _lowercase alpha characters, numeric digits,
underscores, hyphens, or dots_.


Furthermore, all FSL conda packages are prefixed with `fsl-`. An FSL project
with name `<project>` will have a corresponding conda package name of
`fsl-<project>`. For FSL projects with a name that begins with `fsl`
(e.g. `fslvbm`, `fsl_deface`), the leading `fsl` will be dropped in the
construction of the corresponding conda-package name. For example:


| **FSL project name** | **Conda package name** |
| -------------------- | ---------------------- |
| `avwutils`           | `fsl-avwutils`         |
| `fslvbm`             | `fsl-vbm`              |
| `fsl_deface`         | `fsl-deface`           |
| `fsl-mrs`            | `fsl-mrs`              |
| `NewNifti`           | `fsl-newnifti`         |


There are a small number of exceptions to the above conventions. For example,
the [`fdt`](https://git.fmrib.ox.ac.uk/fsl/fdt) project is built into two
conda packages - the [`fsl-fdt`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt)
package, providing CPU-only executables, and the
[`fsl-fdt-cuda`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt-cuda) package,
providing GPU/CUDA-enabled executables.


## A note on FSL CUDA projects


Most FSL CUDA projects usually provide both GPU-enabled and CPU-only
executables. For example, the [`fsl/fdt`](https://git.fmrib.ox.ac.uk/fsl/fdt)
provides a range of CPU-only executables, including `dtifit` and `vecreg`, in
addition to providing GPU-enabled executables such as `xfibres_gpu`.


To accommodate this convention, multiple conda recipes are used for these
"hybrid" projects. For example, packages for the `fsl/fdt` project are built
from two separate recipes:


 - [`fsl/conda/fsl-fdt`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt), which
   builds the CPU-only executables - these recipes are built as `linux` and
   `macos` packages.
 - [`fsl/conda/fsl-fdt-cuda`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt-cuda),
   which builds the GPU-enabled executables - these recipes are built as
   `linux-cuda-X.Y` packages.


## Automatically generate a conda recipe for a FSL project


The fsl/conda/fsl-ci-rules> project contains a utility script called
`create_conda_recipe` that can be used to automatically generate an initial
version of a recipe for a FSL project that is hosted on GitLab. This is a
useful way to start, as the `create_conda_recipe` script will generate a
recipe with a standardised structure, and with most information already
populated.


To generate an initial version of a conda recipe for a new project:


1. Make sure you have FSL installed and active in your shell environment.

2. Create a [GitLab personal access
   token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-tokens),
   which will allow you to programmatically interact with GitLab.

3. Install `fsl/conda/fsl-ci-rules` into your `$FSLDIR` (or into any
   `python>=3.7` environment):

    ```
    $FSLDIR/bin/pip install git+https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules.git
    ```

4. Call the script to generate an initial version of the recipe:

   ```
   create_conda_recipe -t <token> -pp <project_path> ./recipe
   ```

   where:
    - `<token>` is you GitLab personal access token
    - `<project_path>` is the GitLab repository path of your project (e.g.
      `fsl/avwutils`, `fsl/newimage`, etc), or the full URL to a git
      repository.


The `create_conda_recipe` script can optionally push the recipe repository to
GitLab, within the [fsl/conda](https://git.fmrib.ox.ac.uk/fsl/conda)
namespace, and named according to the FSL conda package naming conventions
outlined above. For example, this command command:


```
create_conda_recipe -t <token> -pp paulmc/my_new_project -p ./recipe
```


will create a conda recipe for a GitLab project repository at
`https://git.fmrib.ox.ac.uk/paulmc/my_cool_project`, for a conda package
called `fsl-my_cool_project`, and will upload the recipe to a GitLab
repository at `https://git.fmrib.ox.ac.uk/fsl/conda/fsl-my_cool_project`.


The `create_conda_recipe` script has a few options allowing you to control its
behaviour (e.g. create a recipe repository locally insteaad of on GitLab,
create the recipe from a branch other than `master` from the project
repository); run `create_conda_recipe -h` for help on the available options.


## Create a conda recipe for a FSL project by hand


We recommend either using the `create_conda_recipe` script as outlined above,
or copying an existing recipe, to generate an initial version of the recipe
for your FSL project. This has the advantage that recipes for all FSL projects
will follow a few simple conventions and standards. However, it is possible to
create a conda recipe by hand.


The purpose of this section is not to provide full instructions on writing a
conda recipe, but rather on the specific issues that need to be considered
when writing a conda recipe for a FSL project. More general information on
creating conda recipes can be found at these websites:


 - https://docs.conda.io/projects/conda-build/en/latest/resources/define-metadata.html
 - https://python-packaging-tutorial.readthedocs.io/en/latest/conda.html


A FSL conda recipe is simply a flat directory containing the following files:

 - `meta.yaml`: A YAML file which contains a description of the conda package,
   including its name, current version, URL to the project repository, and
   list of dependencies.

 - `build.sh`: A bash script which is executed in order to build the
   package. For FSL C++ projects, this script essentially just calls
   `$FSLDIR/etc/fslconf/fsl-devel.sh`, then runs `make` and `make
   install`. For FSL `Makefile`-based projects, `build.sh` is required, but
   for Python-based projects, `build.sh` may or may not be necessary.

 - `post-link.sh`: For projects which provide executables that are installed
   into `$FSLDIR/bin/`, this script is used to conditionally create wrapper
   scripts in `$FSLDIR/share/fsl/bin/`, as outlined in
   [`official_fsl_installations.md`](official_fsl_installations.md). This
   script is not required for projects which do not provide any executables.

 - `pre-unlink.sh`: This script is called when a package is *uninstalled* -
   its job is to remove any wrapper scripts that were created by
   `post-link.sh`. This script is not required for projects which do not
   provide any executables.


FSL projects are broadly divided into one of the following categories:

  - **`Makefile`-based project**: FSL projects which use a FSL-style
    `Makefile` to compile and install their deliverables (shared libraries,
    scripts, and compiled executables).

  - **`Makefile`-based CUDA project**: FSL projects which use a FSL-style
    `Makefile`, and which provide GPU-accelerated executables linked against
    CUDA.

  - **`setup.py`-based project**: FSL projects which are written in Python,
    and which have a `setup.py` file which is used to build the project as
    a Python package.


The mechanisms by which projects in these categories are built are slightly
different and, therefore, the conda recipe for projects from different
category will look slightly different. Examples of `Makefile`-based and
`setup.py`-based FSL projects, and associated conda recipes, can be
respectively found in the `examples/cpp`, `examples/cuda`, and
`examples/python` sub-directories. Some important details are highlighted
below.


### Writing the `meta.yaml` file


The `meta.yaml` file contains metadata about your project, including:

 - The conda package name
 - The URL of the project git repository
 - The version number
 - The build number (used in case the conda package for a single version
   needs to be re-built for some reason).
 - A list of the package build- and run-time dependencies.


**Essential metadata** In order to allow for automatic maintenance of FSL
conda recipes, you should define the essential metadata (name, version, etc)
as `jinja2` variables using the following syntax:


```
{% set name       = '<conda-package-name>' %}
{% set version    = '<version-number>'     %}
{% set repository = '<repository-url>'     %}
{% set build      = '0'                    %}
```


Then use these variables within the recipe YAML, e.g.:


```
package:
  name:    {{ name    }}
  version: {{ version }}
```


By following this convention, FSL conda recipes can be automatically updated
when a new version of a project is released.


**Project repository and git revision** The automated CI rules defined in
`fsl-ci-rules` allow the project repositoy and git revision (tag, branch, etc)
used to build a conda package to be overridden via the `FSLCONDA_REPOSITORY`
and `FSLCONDA_REVISION` environment variables. To facilitate this, the project
source repository and git revision must be specified in the `meta.yaml` like
so (remembering that we have defined `repository` and `version` as `jinja2`
variables, above):


```
source:
  # the FSLCONDA_REPOSITORY and FSLCONDA_REVISION
  # environment variables can be used to override
  # the repository/revision for development purposes.
  git_url: {{ os.environ.get("FSLCONDA_REPOSITORY", repository) }}
  git_rev: {{ os.environ.get("FSLCONDA_REVISION",   version)    }}
```


Following this convention allows conda packages for development and testing to
be built, both automatically, and when developing/testing locally, simply by
setting the `FSLCONDA_REPOSITORY` and `FSLCONDA_REVISION` variables.


**run_exports (C/C++ projects only)** When compiling a C/C++ project, any
shared library dependencies of the project must be present at the time of
compilation, and at run time. This means that the dependencies of your project
may need to be listed twice within the `requirements` section - once under
`host` (or `build`), and again under `run`.  We can avoid having to list
dependencies twice by specifying `run_exports` in the dependency recipes.


The
[`run_exports`](https://docs.conda.io/projects/conda-build/en/latest/resources/define-metadata.html#export-runtime-requirements)
section is a trick which can be used within `meta.yaml`, which essentially
allows us to define a dependency as a build-time dependency, and have it
automatically propagated as a run-time dependency.


`run_exports` also allows us to specify the ABI compatibilty guarantees of a
project.  Maintainers of a C/C++ library must consider ABI compatibility
across different versions of the library. For FSL C/C++ projects which follow
the `YYMM.X` versioning scheme (described in the [development
workflows](development_workflows.md) page), different releases within a single
`YYMM` series must preserve ABI compatibility.


The ABI compatibility guarantee that a particular project promises can be
encoded in the `run_exports` section of the project conda recipe, by using the
`pin_subpackages(name, max_pin)` macro function. As an example, for a project
**`A`** which follows the `major.minor.patch` versioning scheme, setting
`max_pin` to `'X.X'` specifies that different releases of project **`A`** with
the same `major.minor` version are ABI-compatible. For another package **`B`**
which is dependent on **`A`**, `conda` will ensure that, when **`B`** is
installed, an ABI-compatible version of **`A`** will be installed alongside
**`B`**.


For FSL projects which follow the `YYMM.X` versioning scheme, `max_pin` should
be set to `'X'`, which indicates that different releases with the same `YYMM`
prefix are ABI-compatible.


So if you are writing a conda recipe for a C/C++ project which provides shared
library files that will be used by other projects, add a `run_exports` section
to the `build` section, and set `max_pin` according to the versioning scheme used
for the project, like so:


```
build:
  number: {{ build }}
  run_exports:
    strong:
      - {{ pin_subpackage(name, max_pin='X') }}
```


**`noarch` and `script` (`setup.py`-based projects only)** Conda recipes for
Python `setup.py`-based projects are built slightly differently to native
projects, as the Python `setuptools` machinery is integrated into the conda
build process. For `setup.py`-based projects, the build command is usually
specified within the `build` section of the `meta.yaml` file. Furthermore, if
you are packging a *pure* Python project, with no natively compiled code or
extensions, you must label your recipe as being of type `noarch: python` -
this is also specified within the `build` section. So a typical `build`
section for a `setup.py`-based project will resemble the following:


```
build:
  number: {{ build }}
  noarch: python
  script: {{ PYTHON }} -m pip install . --no-deps --ignore-installed --no-cache-dir -vvv
```


**Requirements** The [`fsl/base`](https://git.fmrib.ox.ac.uk/fsl/base) project
provides the fundamental elements of the contents of a FSL installation,
including FSL initialisation scripts and the `Makefile` machinery. As such it
must be installed in order to build `Makefile`-based FSL projects, and must be
present at run time for most FSL commands to function. All FSL conda recipes
must therefore list `fsl-base` as a requirement. C/C++ projects will also need
to have a C++ compiler installed at build time. For example:


```
requirements:
  build:
    - {{ compiler('cxx') }}
    - make
  host:
    - fsl-base
```


### Recipes for CUDA projects


> **Note:** Mechanisms for building CUDA packages on macOS do not currently
> exist.


Separate packages may be created from `Makefile`-based CUDA projects for
each supported CUDA version. For example, multiple packages are built
from the [`fsl-fdt-cuda`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt-cuda)
recipe - each package is called `fsl-fdt-cudaX.Y`, where `X.Y` denotes the
CUDA version that the package was built against.


To enable this, the `meta.yaml` file for a CUDA project needs to contain a
couple of additional elements. First, when a CUDA package is built, a variable
called `CUDA_VER`, containing the `major.minor` CUDA version the package is
being built against, **must** be set in the environment.  The `meta.yaml` file
can read this environment variable in as a `jinja2` variable like so:


```
{{ '{% set cuda_version = os.environ["CUDA_VER"] %}' }}
```


Built CUDA packages must be named according to the version of CUDA they were
built against, with a suffix of the form `-cuda-X.Y` . This is accomplished by
adding the CUDA version to the package name like so:


```
{% set cuda_label   = '-cuda-' + cuda_version %}
{% set name         = 'fsl-fdt' + cuda_label %}
```


When a CUDA package is built, the CUDA toolkit and `nvcc` compiler must be
installed in the build environment independently of conda, but the C++
compiler should be installed via conda.  One complication which needs to be
addressed is that different versions of `nvcc` are compatible with different
versions of `gcc`, so the version of `gcc` that should be installed depends on
the version of CUDA against which the package is being built.


To handle this complication, a template `requirements` section for a CUDA
project needs to look something like this:


```
requirements:
  host:
    - fsl-base
  build:
    # Different versions of nvcc need
    # different versions of gcc. This is
    # outlined in the "System requirements"
    # section of the CUDA installation
    # guide for each CUDA version, e.g.:
    # https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html
    {% if   cuda_version in ("9.2", "10.0") %}
    - {{ compiler("cxx") }} 7.*  # [linux]
    {% elif cuda_version in ("10.1", "10.2") %}
    - {{ compiler("cxx") }} 8.*  # [linux]
    {% elif cuda_version in ("11.0", "11.1", "11.2", "11.3") %}
    - {{ compiler("cxx") }} 9.*  # [linux]
    {% elif cuda_version in ("11.4", "11.5", "11.6") %}
    - {{ compiler("cxx") }} 11.*  # [linux]
    {% else %}
    - {{ compiler("cxx") }} # [linux]
    {% endif %}
    - make
```


### Defining the build proceess


If you are writing a recipe for a `Makefile`-based FSL project, you need to
write a `build.sh` script which uses the FSL `Makefile` machinery to build
your project. A typical `build.sh` script will resemble the following:


```
#!/usr/bin/env bash

# $PREFIX is the installation destination
# when a conda package is being built
export FSLDIR=$PREFIX

# Configure the build environment
. $FSLDIR/etc/fslconf/fsl-devel.sh

# Inject FSL copyright boilerplate
# into project source code
make insertcopyright

# Install project source code into
# $PREFIX/src/ (a.k.a. $FSLDIR/src/)
mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME

# Build and install the project
make
make install
```


If you are writing a recipe for a `setup.py`-based FSL project, a `build.sh`
script is generally not required.


#### Projects which use `git lfs`


Projects which use `git lfs` need to perform some initialisation steps in
their ``build.sh`` script. This is not performed automatically for a couple of
reasons:

 - Conda does not fully support building packages from git repositories with
   LFS files (although this may change in the [near
   future](https://github.com/conda/conda-build/pull/4318).
 - We use shared physical machines for building macOS packages, where
   modifying the global or user git configuration files is problematic.

For these reasons, the conda recipe for a project which uses `git lfs` differs
slightly from a project which does not use `git lfs`:

1. The `meta.yaml` file should **not** contain a `source` section specifying
   the project git repository and revision.
2. Instead, the `build.sh` file should clone the repository and check out the
   appropriate revision, and also initialise git LFS and checkout LFS files
   (if they are needed to build the package). An example `build.sh` script
   for LFS repositories may look like this:

   ```
   # The PKG_VERSION environment variable
   # will be set to the value of the
   # meta.yaml package: version: field

   version=${PKG_VERSION}
   repository=https://git.fmrib.ox.ac.uk/fsl/data_standard.git

   # Clone the repository, but don't
   # download lfs files yet (in case
   # git-lfs is already installed)
   GIT_LFS_SKIP_SMUDGE=1 git clone ${repository}

   cd data_standard
   git checkout ${version}

   # Download lfs files for ${version}
   git lfs install --force --local
   git lfs fetch
   git lfs checkout

   # Build the package
   ```


#### Notes on CUDA projects


A critical requirement of "hybrid" FSL CUDA projects, which provide both
CPU-only and GPU-capable executables, is that the project `Makefile` must be
able to conditionally compile only the CPU components, **or** the GPU
components, provided by the project.


The specific `make` invocations that need to be made depend on how the project
`Makefile` is written. For example, the
[`fdt`](https://git.fmrib.ox.ac.uk/fsl/fdt) `Makefile` accepts `cpu` and `gpu`
flags, to control which parts of the project are compiled - `make cpu=1` will
compile only the CPU components of the `fdt` project, whereas `make cpu=0
gpu=1` will compile only the GPU components.


The `build.sh` scripts for the
[`fsl-fdt`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt) and
[`fsl-fdt-cuda`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt-cuda) take
advantage of this mechanism, so that the `fsl-fdt` recipe will only compile
the CPU components, and the `fsl-fdt-cuda` recipe will only compile the GPU
components


The fdt `Makefile` generates **labelled** binaries - CUDA executables and
shared libraries are named according to the version of CUDA they were compiled
against, e.g. `xfibres_gpu9.2`. This is important so that different variants
of the `xfibres_gpu` executable, compiled against different CUDA versions, can
be installed alongside each other in the same environment.


The FSL `Makefile`-based build system allows CUDA binaries to either
statically or dynamically link against the CUDA Toolkit.  Dynamic linking is
the default behaviour - this produces smaller binary file sizes, and may be
preferable for local development.


However static linking is preferable for releases, as it allows binaries to be
used without requiring the CUDA Toolkit to be installed. Static linking can be
enabled by passing the `CUDA_STATIC=1` variable to the `make` command.


The `build.sh` script for the `fsl-fdt` recipe therefore looks the same as the
template `build.sh` script above, except the `make` invocations are as
follows:


```
make cpu=1
make cpu=1 install
```


And the `make` invocations in the `build.sh` script for the `fsl-fdt-cuda`
recipe are as follows:


```
make CUDA_STATIC=1 cpu=0 gpu=1
make CUDA_STATIC=1 cpu=0 gpu=1 install
```


### `post-link.sh` and `pre-unlink.sh` scripts.


Official/full FSL installations contain wrapper scripts for every FSL
executable in `$FSLDIR/share/fsl/bin/` - this is so that a user can add FSL
commands to their `$PATH` via this directory, rather than the `$FSLDIR/bin/`
directory, and avoid adding all of the other executables in `$FSLDIR/bin/` to
their `$PATH` (e.g. `python`).


These wrapper scripts are created/removed by two utility commands which are
provided by the `fsl-base` package - `createFSLWrapper` and
`removeFSLWrapper`. The conda recipes for any FSL projects which provide
executables need to call these scripts at the time of
installation/uninstallation to ensure that wrapper scripts for the executables
are created/removed.


This can be achieved by using [`post-link.sh` and
`pre-unlink.sh`](https://docs.conda.io/projects/conda-build/en/latest/resources/link-scripts.html)
scripts, which are conda-specific mechanisms allowing custom logic to be
executed when a conda package is installed or uninstalled.


For a FSL project which provides executables called `fsl_command1`,
`fsl_command2` and `fsl_command3`, the `post-link.sh` for the project recipe
should contain:


```
if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper fsl_command1 fsl_command2 fsl_command3
fi
```


The corresponding `pre-unlink.sh` script should contain:


```
if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/removeFSLWrapper fsl_command1 fsl_command2 fsl_command3
fi
```
