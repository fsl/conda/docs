#include <iostream>

#include "example_cpp_lib.h"

int main(int argc, char *argv[]) {

  if (argc < 2) {
    std::cout << "Usage: example_cpp_exe name" << std::endl;
    return 1;
  }

  helloworld(argv[1]);

  return 0;
}
