#include <string>
#include <iostream>

#include "example_cpp_lib.h"

void helloworld(std::string s) {
  std::cout << "Hello, " << s << std::endl;
}
