#include <iostream>

#include "example_cuda_lib.h"

int main(int argc, char *argv[]) {

  if (argc < 2) {
    std::cout << "Usage: example_cuda_exe name" << std::endl;
    return 1;
  }

  helloworld(argv[1]);

  return 0;
}
