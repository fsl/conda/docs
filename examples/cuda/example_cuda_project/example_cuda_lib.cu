#include <string>
#include <iostream>

#include "example_cuda_lib.h"

void helloworld(std::string s) {

  int   driverVersion;
  int   runtimeVersion;
  char *memory;

  std::cout << "Hello, " << s << std::endl;

  if (cudaDriverGetVersion(&driverVersion) != 0) {
    std::cout << "Error querying CUDA driver version!" << std::endl;
  }

  std::cout << "CUDA driver version: " << driverVersion << std::endl;

  if (cudaRuntimeGetVersion(&runtimeVersion) != 0) {
    std::cout << "Error querying CUDA runtime version!" << std::endl;
    return;
  }

  std::cout << "CUDA runtime version: " << runtimeVersion << std::endl;

  if (cudaMalloc(&memory, 1024) != 0) {
    std::cout << "Error allocating memory!" << std::endl;
  }

  if (cudaFree(memory) != 0) {
    std::cout << "Error freeing memory!" << std::endl;
  }

  std::cout << "Successfully allocated memory on GPU" << std::endl;
}
