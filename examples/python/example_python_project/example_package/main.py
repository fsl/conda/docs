#!/usr/bin/env python

import os.path as op
import sys


def main():

    scriptname = op.basename(sys.argv[0])

    if len(sys.argv) != 2:
        print(f'Usage: {scriptname} name')
        sys.exit(1)

    print(f'Hello, {sys.argv[1]}')


if __name__ == '__main__':
    main()
