#!/usr/bin/env python


from setuptools import setup


setup(name='example_python_project',
      version='0.0.1',
      description='Example FSL Python project',
      author='Paul McCarthy',
      install_requires=['numpy', 'nibabel'],
      packages=['example_package'],
      entry_points={
          'console_scripts' : [
              'example_python_exe = example_package.main:main',
          ]
      }
)
