# FSL releases management


This page describes release management procedures for the FMRIB Software
Library (FSL). Release management procedures for individual projects within
FSL are described [separately](development_workflow.md).


## FSL versioning scheme


FSL releases are given a version number containing three or four numbers, e.g.
`6.0.6`, `6.0.6.2` etc. Each of the numbers in this `X.Y.Z[.W]` scheme are
referred to as follows:

 - `X`: _Major_ version
 - `Y`: _Minor_ version
 - `Z`: _Patch_ version
 - `W`: _Hotfix_ version

Due to licensing restrictions, the _major_ and _minor_ versions are only
changed when substantial scientific additions or changes are made to the FSL
code base. So for many FSL versions, only the _patch_ and _hotfix_ versions
are updated.

Semantically, the _patch_ and _hotfix_ version numbers are used as follows:

 - The _patch_ version is updated when substantial non-scientific additions
   or enhancements are made to the code base, or when major updates are made
   to the underlying dependencies of the FSL code base.
 - The _hotfix_ version is updated for releases involving minor changes and
   bugfixes.


## FSL releases


FSL releases are managed at the fsl/conda/manifest> repository.


The `fsl-release.yml` file contains a list of all FSL projects and version
numbers which comprise a FSL installation. As new versions of individual FSL
projects are released, the entries for those projects are updated in the
`fsl-release.yml` file.


Two separate branche are maintained on the fsl/conda/manifest> repository:

 - The `master` branch is updated regularly, and is used to generate files for
   internal releases.

 - The `external` branch is updated when a new public version of FSL is
   released. When preparing for a new release, the `master` branch is
   merged into the `external` branch.


A new version of FSL can be released by creating a tag on the `external` branch
of the fsl/conda/manifest> repository. This will trigger a CI job which
generates a set of conda `environment.yml` files from the `fsl-release.yml`
package list, one for each supported platform. These are then published to the
[FSL release web
page](https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/), and so will
be available to the [`fslinstaller.py`
script](https://git.fmrib.ox.ac.uk/fsl/conda/installer).


In brief, a new version of FSL is released like so:

1. The `fsl-release.yml` file on the `external` branch is updated with
   versions of all packages that are to be included in the new release.

2. A new tag is created on the fsl/conda/manifest> repository; the tag must
   be the version number of the new FSL release, e.g. `6.0.7`.

3. In response to the new tag, a series of jobs are automatically executed
   on the fsl/conda/manifest> repository using GitLab CI / CD:

    a) A set of conda `environment.yml` files are generated, one for each
       supported platform/OS.

    b) A file called `manifest.json` is generated - this file contains all of
       the metadata needed to install FSL on a target system - it is used by
       the `fslinstaller.py` script when FSL is installed. The `manifest.json`
       file contains information about all available FSL releases - it is
       re-generated every time a new version is released.

    c) The `environment.yml` and `manifest.json` files are published to
       the [FSL release
       page](https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/).
       This step must be invoked manually, by starting the `publish-release`
       CI job.


## Release candidates


When preparing for a new FSL release or internal installation, it is often
desirable to publish a _release candidate_ (RC) for testing and evaluation
purposes. A release candidate can be published like so:

1. Create a release candidate (RC) branch starting from the `external`
   branch of the fsl/conda/manifest> repository, e.g. `rel/6.0.7`.

2. Merge changes from the `master` branch into the RC branch. Resolve any
   conflicts that arise, making sure that internal-only FSL packages are not
   included in the package lists. A good idea is to compare your branch with
   the `master` branch - usually the only difference should be that your
   branch does not contain any internal-only packages:
   ```
   git diff origin/master HEAD
   ```

3. Publish the `environment.yml` and `manifest.json` files that are
   generated from that branch. The `manifest.json` file will be named and
   published as `manifest-<last-tag>.<YYYYMMDD>.<commit>.<branch>.json`,
   e.g.  `manifest-6.0.6.3.20230325.5ab3fe2.rel-6-0-7.json`

4. Use the
   [`fslinstaller.py`](https://git.fmrib.ox.ac.uk/fsl/conda/installer)
   script to install the release candidate, e.g.:

       wget https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/manifest-6.0.6.3.20230325.5ab3fe2.rel-6-0-7.json
       python fslinstaller.py --manifest manifest-6.0.6.3.20230325.5ab3fe2.rel-6-0-7.json

   Or use `conda`/`mamba` directly with the corresponding `environment.yml`
   files, e.g.:

       wget https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/fsl-6.0.6.3.20230325.5ab3fe2.rel-6-0-7_linux-64.yml
       mamba env create -p fsl-6.0.7rc -f fsl-6.0.6.3.20230325.5ab3fe2.rel-6-0-7_linux-64.yml

5. Test the installation.

6. If any changes are required, make them via a separate MR into the
   `master` branch with the changes.

7. Repeat steps 2-6 as needed.

8. When you are happy with the RC, merge the release candidate branch
   into the `external` branch, and tag the new release.

9. Publish the `environment.yml` and `manifest.json` files that are generated
   from the tag.

> The process described above is used for the following reasons:
>   - All changes to the manifest should occur via the `master` branch, to
>     ensure that the `master` branch is always ahead of, and in sync with,
>     the `external`  branch.
>   - The final state of the RC branch, before it is merged into `external`
>     should be in exactly the same state as (what will become) the tagged
>     release; this is to ensure that the FSL release which undergoes testing
>     is identical to that which is to be released.
>   - Every merge into the `external` branch should correspond to a tagged
>     release; this is so that the commit and branching history of the
>     fsl/conda/manifest> repository remains easy to reason about.


## Associating FSL project versions with FSL release versions


To identify the version of a specific FSL project that was released as part of
a specific FSL version:

 1. Go to the
    [`fsl/conda/manifest`](https://git.fmrib.ox.ac.uk/fsl/conda/manifest/)
    repository.
 2. Select the FSL version tag of interest, and open the `fsl-release.yml`
    file.
 3. Look up the version for the project.


## FSL release management before FSL 6.0.6


Prior to FSL 6.0.6, FSL releases were managed at the
[`fsl/FslBuildManifests`](https://git.fmrib.ox.ac.uk/fsl/FslBuildManifests/)
repository. The `FSL_SourceManifest.yaml` file contained a list of source code
repositories and revisions of all FSL projects which comprised a FSL
installation. New FSL releases were denoted by a tag on the
`fsl/FslBuildManifests` repository.


The bulk of FSL was built monolithically, by hand, using scripts located in
the [`fsl/distbase`](https://git.fmrib.ox.ac.uk/fsl/distbase) and
[`mwebster/FslBuilds`](https://git.fmrib.ox.ac.uk/mwebster/FslBuilds/)
repositories.


FSL projects and dependencies installed into the embedded `fslpython` conda
environment were built and released independently to the
https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/channel/ conda channel, or to
`conda-forge`. The versions of these packages were managed at the
[`fsl/post_install`](https://git.fmrib.ox.ac.uk/fsl/post_install/) repository,
in the `fslpython_environment.yml` file.


For FSL versions prior to 6.0.6, the process followed to identify the version
of a specific FSL project that was released as part of a specific FSL version
differs depending on whether the project was part of the "core" FSL release,
or whether the project was part of the embedded `fslpython` conda environment.
For "core" projects, the process is similar to that described above:

1. Go to the
   [`fsl/FslBuildManifests`](https://git.fmrib.ox.ac.uk/fsl/FslBuildManifests/)
   repository.
2. Select the FSL version tag of interest, and open the
   `FSL_SourceManifest.yml` file.
3. Look up the version for the project of interest.


For projects installed in the `fslpython` environment, follow the above steps
for the `fsl/post_install` project, and then:

1. Go to the
   [`fsl/post_install`](https://git.fmrib.ox.ac.uk/fsl/post_install/)
   repository.
2. Select the `fsl/post_install` version tag of interest, and open the
   `fslpython_environment.yml` file.
3. Look up the version for the project of interest.
