# Setting up a local FSL development environment


*This document is written under the assumption that you have FSL 6.0.6 or
newer, installed via the fslinstaller script. These instructions will not work
for older versions of FSL, nor for manually managed FSL installations.*


This document describes how to set up a local development environment for
compiling FSL C/C++ projects. It does **not** describe how to build conda
packages for FSL projects - this is described in
[`building_fsl_conda_packages.md`](building_fsl_conda_packages.md).


A standard FSL installation comes with a C/C++ compiler and the `Makefile`
machinery required to compile FSL C/C++ projects. FSL C/C++ conda packages are
compiled using the [conda-forge](https://conda-forge.org/) compiler toolchain,
and it is recommended that the same compiler toolchain is used for local
development.


> *NOTE:* You can use your operating system compiler if you wish, but be
> warned that doing so will potentially compile your code in a different way
> to how the official FSL conda packages have been compiled.


When you want to work on a FSL project, you can follow these steps:


```bash
# Configure your environment for FSL
# development (if not already done so
# in your default shell environment).
#
# Set FSLDEVDIR to a different directory
# from FSLDIR so that your locally
# compiled libraries and executables do
# not overwrite the stable versions in
# FSLDIR.
export FSLDIR=${HOME}/fsl/
export FSLDEVDIR=${HOME}/fsl-dev/
source $FSLDIR/etc/fslconf/fsl-devel.sh

# Activate the $FSLDIR conda environment
source $FSLDIR/bin/activate base

# Create a copy of the source code for
# the project you want to compile. The
# code that gets installed into 
# $FSLDIR/src/ is the version that is
# used to generate the binaries in your
# FSL installation
cp -r $FSLDIR/src/fsl-randomise ./randomise
cd randomise

# OR

# Get a copy of the latest version of 
# the source code for the project you 
# are working on from the FMRIB gitlab
git clone https://git.fmrib.ox.ac.uk/fsl/randomise.git
cd randomise

# compile and install. Executables/libraries
# will be installed into $FSLDEVDIR, so your
# main FSL installation will not be affected.
make
make install
```

## Compiling CUDA projects


If you are building a CUDA project, you need to:

1. Install the CUDA Toolkit
2. Ensure that the `<cuda-toolkit-location>/bin/` directory is on your `$PATH`
3. Ensure that the correct version of GCC is installed.

At the moment, compilation of CUDA projects is only supported on linux-64.

Note that you do not need a GPU in order to compile CUDA code.


At the moment, we are **not** using versions of the CUDA Toolkit that are
published on anaconda.org.  This is so that FSL installations are not tightly
coupled to a specific version of CUDA. Instead, we recommend downloading and
installing the CUDA Toolkit from https://developer.nvidia.com/cuda-toolkit.


Note that it is possible to have multiple versions of the CUDA Toolkit
installed alongside each other - you simply need to keep track of where each
version is installed (e.g. `/usr/local/cuda-10.2`, `/usr/local/cuda-11.0`,
etc).


You also need to ensure that you have an appropriate version of GCC for the
CUDA version you are compiling against. Different versions of the CUDA toolkit
require different versions of GCC, as outlined in the following table:


| CUDA Toolkit version | Required GCC version |
|----------------------|----------------------|
| 9.2                  | 7                    |
| 10.0                 | 7                    |
| 10.1                 | 8                    |
| 10.2                 | 8                    |
| 11.0                 | 9                    |
| 11.1                 | 9                    |
| 11.2                 | 9                    |
| 11.3                 | 9                    |
| 11.4                 | 11                   |
| 11.5                 | 11                   |
| 11.6                 | 11                   |
| 11.7                 | 11                   |


To compile FSL code with a different compiler version than that which is
installed in `$FSLDIR`, you have the option of creating a separate
conda environment with the appropriate GCC version. For example:

    $FSLDIR/bin/conda create -c conda-forge  -p ~/gcc8 "gxx_linux-64=8.*" make

Then in order to use this environment for compiling FSL projects:

    # Configure your environment for FSL development
    export FSLDIR=${HOME}/fsl
    export FSLDEVDIR=${HOME}/fsldev
    source $FSLDIR/etc/fslconf/fsl-devel.sh

    # Activate the compiler toolchain environment
    source $FSLDIR/bin/activate ~/gcc8

    # compile your code
    cd eddy
    make cuda=1


If you need to compile against different versions of CUDA, you can
create a separate conda environment for each version of GCC that you
will need.
