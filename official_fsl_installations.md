# Official / full FSL installations


An official FSL installation is a self-contained directory installed on an end
user's system, containing the full collection of FSL commands. An official FSL
installation is typically located at `${HOME}/fsl/` - the FSL installation
directory is identified by the `$FSLDIR` environment variable. The `$FSLDIR`
directory is a
[**miniconda**](https://docs.conda.io/en/latest/miniconda.html#) environment -
all FSL projects are installed directly into the base miniconda environment
at `$FSLDIR`.


> An FSL installation may be based on [miniforge or
> mambaforge](https://github.com/conda-forge/miniforge), which are variants of
> miniconda tailored specifically for conda-forge.


An official (`fslinstaller`-installed) FSL installation can be distinguished
from a manually installed/managed FSL installation by the presence of a file
called `$FSLDIR/etc/fslversion`. This file is created by the `fslinstaller.py`
script, and is a plain-text file which contains the installed FSL version
identifier.


Because `$FSLDIR` is a conda environment into which all FSL projects and their
dependencies are installed, `$FSLDIR/bin/` will contain executable files
provided by both FSL, and by its dependencies. This can give rise to a problem
whereby executables in `$FSLDIR/bin/` can mask other executables of the same
name which are also installed in the user's environment, but come after
`$FSLDIR/bin/` on the user's `$PATH` (e.g. `python`, `conda`, etc).


To work around this problem, and to isolate all of the FSL executables from
the other executables installed into the same environment, the conda packages
for all FSL projects use [`post-link.sh` and
`pre-unlink.sh`](https://docs.conda.io/projects/conda-build/en/latest/resources/link-scripts.html)
scripts to create _wrapper scripts_ within
`$FSLDIR/share/fsl/bin/`<sup>*</sup>. These wrapper scripts are created by the
[`$FSLDIR/share/fsl/sbin/createFSLWrapper`](https://git.fmrib.ox.ac.uk/fsl/base/-/blob/master/share/fsl/sbin/createFSLWrapper)
script, which is installed as part of the
[`fsl-base`](https://git.fmrib.ox.ac.uk/fsl/base/) package.


> <sup>*</sup> More details on FSL project conda recipes are outlined in
> [`creating_fsl_conda_recipes.md`](creating_fsl_conda_recipes.md).


Users can then add the `$FSLDIR/share/fsl/bin/` directory to their `$PATH`,
with the effect that only FSL executables will be made available in their
environment. This mechanism also has the advantage that all existing FSL and
third-party scripts which refer to `$FSLDIR/bin/<command>` will continue to
work without modification.


Once an official FSL release has installed onto a user's system, they can
configure their environment to use FSL as follows (for a `bash`-compatible
shell):


```
export FSLDIR=/usr/local/fsl/
source $FSLDIR/etc/fslconf/fsl.sh
```


For a typical installation, this environment configuration is automatically
added to the user's iniitalisation script by the `fslinstaller.py` script.
